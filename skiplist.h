#ifndef __SKIPLIST_H
#define __SKIPLIST_H

#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>

struct skiplist_node_t {
    int val;
    struct skiplist_node_t *backward;    //后退指针<---
    struct skiplist_level_t {
        struct skiplist_node_t *forward; //前进指针--->
    }level[];    //节点的层
};

struct skiplist_t;

/**
* @brief    创建跳表
* @retval   成功：返回创建的跳表，失败：NULL
*/
struct skiplist_t *skiplist_create(void);

/**
* @brief    在跳表中搜索给定值
* @param    obj： 跳表句柄
* @param    target： 要查找的目标
* @retval   成功：true（找到），失败：false（不存在）
*/
bool skiplist_search(struct skiplist_t *obj, int target);

/**
* @brief    向跳表添加节点
* @param    obj： 跳表句柄
* @param    target： 要添加的节点的值（会覆盖new_node内的值）
* @retval   添加后的节点
*/
struct skiplist_node_t *skiplist_add(struct skiplist_t *obj, int target);

/**
* @brief    从跳表中删除给定的值（若存在重复值，则只删除第一个找到的）
* @param    obj： 跳表句柄
* @param    num： 要删除的节点值
* @param    unlink_node： 如果不为NULL则仅分离该节点，否则释放该节点
* @retval   成功：true（成功删除），失败：false（不存在）
*/
bool skiplist_delete(struct skiplist_t *obj, int num, struct skiplist_node_t **unlink_node);

/**
* @brief    更新跳表中的节点值
* @param    obj： 跳表句柄
* @param    target： 需要更新的节点的值
* @param    new_target： 更新后的节点的值
* @retval   成功：返回更新后的节点，失败：NULL
*/
struct skiplist_node_t *skiplist_update(struct skiplist_t *obj, int target, int new_target);

/**
* @brief    销毁跳表
* @param    obj： 跳表句柄
* @retval   无
*/
void skiplist_destroy(struct skiplist_t *obj);

/**
* @brief    遍历跳表（注意：仅用于调试）
* @param    obj： 跳表句柄
* @retval   无
*/
void skiplist_for_each(struct skiplist_t* obj);

#endif/*__SKIPLIST_H*/
